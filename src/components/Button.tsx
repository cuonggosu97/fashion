import React, { ReactNode } from "react";
import { StyleSheet } from "react-native";
import { RectButton } from "react-native-gesture-handler";
import { useTheme } from "@shopify/restyle";
import { Theme, Text } from "./Theme";

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },

})

interface ButtonProps {
    variant: "default" | "primary" | "transparent";
    label?: string;
    onPress: () => void;
    children?: ReactNode;
}

const Button = ({ variant, label, onPress, children }: ButtonProps) => {
    const theme = useTheme<Theme>();
    const backgroundColor =
        variant === "primary"
            ? theme.colors.primary
            : variant === "transparent"
                ? "transparent"
                : theme.colors.grey;
    const color = variant === "primary" ? theme.colors.white : theme.colors.secondary;
    const width = variant === "transparent" ? undefined : 245;
    const height = variant === "transparent" ? undefined : 50;
    const borderRadius = variant === "transparent" ? undefined : 25;
    return (
        <RectButton
            style={[styles.container, { backgroundColor, width, borderRadius, height }]}
            {...{ onPress }}
        >
            {children ? (
                children
            ) : (
                    <Text variant="button" style={{ color }}>{label}</Text>
                )}
        </RectButton >
    );
};

Button.defaultProps = { variant: "default" };

export default Button;