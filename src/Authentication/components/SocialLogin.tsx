import React, { ReactNode } from "react";
import Svg, { Path } from "react-native-svg"

import { Box, useTheme } from "../../components/";

const Google = () => (
    <Svg viewBox="0 0 48 48" width={24} height={24} >
        <Path
            fill="#FFC107"
            d="M43.611 20.083H42V20H24v8h11.303c-1.649 4.657-6.08 8-11.303 8-6.627 0-12-5.373-12-12s5.373-12 12-12c3.059 0 5.842 1.154 7.961 3.039l5.657-5.657C34.046 6.053 29.268 4 24 4 12.955 4 4 12.955 4 24s8.955 20 20 20 20-8.955 20-20c0-1.341-.138-2.65-.389-3.917z"
        />
        <Path
            fill="#FF3D00"
            d="M6.306 14.691l6.571 4.819C14.655 15.108 18.961 12 24 12c3.059 0 5.842 1.154 7.961 3.039l5.657-5.657C34.046 6.053 29.268 4 24 4 16.318 4 9.656 8.337 6.306 14.691z"
        />
        <Path
            fill="#4CAF50"
            d="M24 44c5.166 0 9.86-1.977 13.409-5.192l-6.19-5.238A11.91 11.91 0 0124 36c-5.202 0-9.619-3.317-11.283-7.946l-6.522 5.025C9.505 39.556 16.227 44 24 44z"
        />
        <Path
            fill="#1976D2"
            d="M43.611 20.083H42V20H24v8h11.303a12.04 12.04 0 01-4.087 5.571l.003-.002 6.19 5.238C36.971 39.205 44 34 44 24c0-1.341-.138-2.65-.389-3.917z"
        />
    </Svg>
);

const Facebook = () => (
    <Svg viewBox="0 0 32 32" width={24} height={24} >
        <Path
            fill="#1976D2"
            d="M19.254 2C15.312 2 13 4.082 13 8.826V13H8v5h5v12h5V18h4l1-5h-5V9.672C18 7.885 18.583 7 20.26 7H23V2.205C22.526 2.141 21.145 2 19.254 2z"
        />
    </Svg>
);

const Apple = () => (
    <Svg viewBox="0 0 26 26" width={24} height={24} >
        <Path
            fill="#000"
            d="M23.934 18.945c-.598 1.325-.883 1.918-1.653 3.086-1.07 1.637-2.586 3.676-4.46 3.688-1.665.015-2.094-1.086-4.356-1.067-2.262.008-2.735 1.086-4.402 1.07-1.872-.015-3.305-1.855-4.38-3.484C1.68 17.664 1.364 12.301 3.22 9.45c1.312-2.023 3.39-3.21 5.344-3.21 1.984 0 3.234 1.09 4.878 1.09 1.594 0 2.563-1.095 4.864-1.095 1.734 0 3.574.946 4.886 2.582-4.296 2.352-3.597 8.489.743 10.13zM16.559 4.406c.836-1.07 1.468-2.586 1.242-4.129-1.367.094-2.961.965-3.895 2.09-.844 1.028-1.543 2.555-1.27 4.031 1.489.047 3.028-.84 3.923-1.992z"
        />
    </Svg>
);

interface SocialIconProps {
    children: ReactNode;
}

const SocialIcon = ({ children }: SocialIconProps) => {
    const theme = useTheme()
    const SIZE = theme.borderRadii.l * 1.5;
    return (
        <Box
            backgroundColor="white"
            width={SIZE}
            height={SIZE}
            borderRadius="l"
            justifyContent="center"
            alignItems="center"
            margin="l"
        >
            {children}
        </Box>
    )
}

const SocialLogin = () => {
    return (
        <Box flexDirection="row" justifyContent="center">
            <SocialIcon>
                <Facebook />
            </SocialIcon>
            <SocialIcon>
                <Google />
            </SocialIcon>
            <SocialIcon>
                <Apple />
            </SocialIcon>
        </Box>
    );
}

export default SocialLogin;