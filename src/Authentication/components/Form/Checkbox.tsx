import React from 'react'
import { Feather as Icon } from '@expo/vector-icons';
import { StyleSheet } from "react-native";

import { Box, Text } from '../../../components';
import { RectButton } from 'react-native-gesture-handler';

interface CheckboxProps {
    label: string;
    checked: boolean;
    onChange: (v: boolean) => void;
}

const Checkbox = ({ label, onChange, checked }: CheckboxProps) => {
    return (
        <RectButton
            onPress={onChange}
            style={{ justifyContent: 'center' }}
        >
            <Box flexDirection="row" alignItems="center">

                <Box
                    marginRight="m"
                    height={20}
                    width={20}
                    justifyContent="center"
                    alignItems="center"
                    borderRadius="s"
                    borderWidth={StyleSheet.hairlineWidth}
                    borderColor="primary"
                    backgroundColor={checked ? "primary" : "white"}
                >
                    <Icon name="check" color="white" />
                </Box>
                <Text variant="button">
                    {label}
                </Text>
            </Box>
        </RectButton>
    )
}

export default Checkbox;
