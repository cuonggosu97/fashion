import React from 'react'
import { Feather as Icon } from '@expo/vector-icons';
import { Routes, StackNavigationProps } from '../components/Navigation'
import { Container, Text, Box, Button } from '../components'
import CloseButton from './components/CloseButton';

const SIZE = 80;

const PasswordChanged = ({
    navigation
}: StackNavigationProps<Routes, "PasswordChanged">) => {
    return (
        <Container footer={
            <Box
                justifyContent="center"
                alignItems="center"
            >
                <CloseButton onPress={() => navigation.pop()} />
            </Box>
        }>
            <Box flex={1} justifyContent="center" alignItems="center" padding="l">
                <Box
                    flex="1"
                    justifyContent="center"
                    alignItems="center"
                >
                    <Box
                        style={{ height: SIZE, width: SIZE, borderRadius: SIZE / 2 }}
                        justifyContent="center"
                        alignItems="center"
                        backgroundColor="primaryLight"
                        marginBottom="xl"
                    >
                        <Text color="primary">
                            <Icon name="check" size={32} />
                        </Text>

                    </Box>
                    <Text variant="title1" textAlign="center" marginBottom="l">
                        Forgot password
            </Text>
                    <Text variant="body" textAlign="center" marginBottom="l">
                        Enter the email address associated with your account
            </Text>
                    <Box alignItems="center">
                        <Button
                            variant="primary"
                            label="Go to Login"
                            onPress={() => navigation.navigate("Login")}
                        />
                    </Box>
                </Box>
            </Box>

        </Container>
    )
}

export default PasswordChanged
