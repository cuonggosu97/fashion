import React, { useRef } from "react";
import { TextInput as RNTextInput } from "react-native";
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { Container, Button, Text, Box } from "../components";
import { StackNavigationProps, Routes } from "../components/Navigation";

import TextInput from "./components/Form/TextInput";
import Footer from "./components/Footer";



const SignUpSchema = Yup.object().shape({
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
    password: Yup.string()
        .min(6, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    passwordConfirmation: Yup.string()
        .equals([Yup.ref("password")], "Password don't match")
        .required('Required'),
});

const SignUp = ({
    navigation
}: StackNavigationProps<Routes, "SignUp">) => {
    const {
        handleChange,
        handleBlur,
        handleSubmit,
        errors,
        touched,
    } = useFormik({
        validationSchema: SignUpSchema,
        initialValues: {
            email: "",
            password: "",
            passwordConfirmation: "",
        },
        onSubmit: (values) => console.log(values)
    });
    const password = useRef<RNTextInput>(null);
    const passwordConfirmation = useRef<RNTextInput>(null);
    const footer = (
        <Footer
            title="Already have an account?"
            action="Login here"
            onPress={() => navigation.navigate("Login")} />
    );
    return (
        <Container {...{ footer }}>
            <Box padding="l">
                <Text variant="title1" textAlign="center" marginBottom="l">
                    Create account
            </Text>
                <Text variant="body" textAlign="center" marginBottom="l">
                    Let's us know what your name, email, and your password
            </Text>
                <Box>
                    <Box marginBottom="m">
                        <TextInput
                            icon="mail"
                            placeholder="Enter your email"
                            keyboardType="email-address"
                            autoCompleteType="email"
                            returnKeyType="next"
                            returnKeyLabel="next"
                            onChangeText={handleChange('email')}
                            onBlur={handleBlur('email')}
                            error={errors.email}
                            touched={touched.email}
                            onSubmitEditing={() => password.current?.focus()}
                        />
                    </Box>
                    <Box marginBottom="m">
                        <TextInput
                            ref={password}
                            icon="lock"
                            placeholder="Enter your password"
                            autoCompleteType="password"
                            returnKeyType="go"
                            returnKeyLabel="go"
                            onChangeText={handleChange('password')}
                            onBlur={handleBlur('password')}
                            error={errors.password}
                            touched={touched.password}
                            onSubmitEditing={() => passwordConfirmation.current?.focus()}
                            secureTextEntry
                        />
                    </Box>
                    <TextInput
                        ref={passwordConfirmation}
                        icon="lock"
                        placeholder="Confirm your password"
                        autoCompleteType="password"
                        returnKeyType="go"
                        returnKeyLabel="go"
                        onChangeText={handleChange('passwordConfirmation')}
                        onBlur={handleBlur('passwordConfirmation')}
                        error={errors.passwordConfirmation}
                        touched={touched.passwordConfirmation}
                        onSubmitEditing={() => handleSubmit()}
                        secureTextEntry
                    />
                    <Box alignItems="center" marginTop="m">
                        <Button
                            variant="primary"
                            label="Create your account"
                            onPress={handleSubmit}
                        />
                    </Box>
                </Box>
            </Box>
        </Container>
    );
}

export default SignUp;
