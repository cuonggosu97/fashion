import React, { useRef } from "react";
import { TextInput as RNTextInput } from "react-native";
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { Container, Button, Text, Box } from "../components";
import { StackNavigationProps, Routes } from "../components/Navigation";

import TextInput from "./components/Form/TextInput";
import Checkbox from "./components/Form/Checkbox";
import Footer from "./components/Footer";

const LoginSchema = Yup.object().shape({
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
    password: Yup.string()
        .min(6, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
});

const Login = ({
    navigation
}: StackNavigationProps<Routes, "Login">) => {
    const {
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
        setFieldValue
    } = useFormik({
        validationSchema: LoginSchema,
        initialValues: { email: "", password: "", remember: true },
        onSubmit: (values) => console.log(values)
    });
    const password = useRef<RNTextInput>(null);
    const footer = (
        <Footer
            title="Don't have an account?"
            action="Sign Up here"
            onPress={() => navigation.navigate("SignUp")} />
    );
    return (
        <Container {...{ footer }}>
            <Box padding="l">
                <Text variant="title1" textAlign="center" marginBottom="l">
                    Wellcome back
            </Text>
                <Text variant="body" textAlign="center" marginBottom="l">
                    Use your credentials below and login to your account
            </Text>
                <Box>
                    <Box marginBottom="m">
                        <TextInput
                            icon="mail"
                            placeholder="Enter your email"
                            keyboardType="email-address"
                            autoCompleteType="email"
                            returnKeyType="next"
                            returnKeyLabel="next"
                            onChangeText={handleChange('email')}
                            onBlur={handleBlur('email')}
                            error={errors.email}
                            touched={touched.email}
                            onSubmitEditing={() => password.current?.focus()}
                        />
                    </Box>
                    <TextInput
                        ref={password}
                        icon="lock"
                        placeholder="Enter your password"
                        autoCompleteType="password"
                        returnKeyType="go"
                        returnKeyLabel="go"
                        onChangeText={handleChange('password')}
                        onBlur={handleBlur('password')}
                        error={errors.password}
                        touched={touched.password}
                        onSubmitEditing={() => handleSubmit()}
                        secureTextEntry
                    />
                    <Box
                        flexDirection="row"
                        justifyContent="space-between"
                        marginVertical="m"
                    >
                        <Checkbox
                            label="Remember me"
                            checked={values.remember}
                            onChange={() => setFieldValue("remember", !values.remember)}
                        />
                        <Button
                            variant="transparent"
                            onPress={() => navigation.navigate("ForgotPassword")}
                        >
                            <Text color="primary">
                                Forgot password
                                    </Text>
                        </Button>
                    </Box>
                    <Box alignItems="center">
                        <Button
                            variant="primary"
                            label="Log into your account"
                            onPress={handleSubmit}
                        />
                    </Box>
                </Box>
            </Box>
        </Container>
    );
}

export default Login;
