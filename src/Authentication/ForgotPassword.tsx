import React from 'react'
import { Linking } from 'react-native'
import { useFormik } from 'formik';
import * as Yup from 'yup';

import Footer from './components/Footer';
import { Routes, StackNavigationProps } from '../components/Navigation';
import { Container, Box, Button, Text } from '../components';
import TextInput from './components/Form/TextInput';


const ForgotPasswordSchema = Yup.object().shape({
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
});

const ForgotPassword = ({
    navigation
}: StackNavigationProps<Routes, "ForgotPassword">) => {
    const {
        handleChange,
        handleBlur,
        handleSubmit,
        errors,
        touched,
    } = useFormik({
        validationSchema: ForgotPasswordSchema,
        initialValues: { email: "" },
        onSubmit: (values) => {
            navigation.navigate("PasswordChanged")
            console.log(values)
        }
    });
    const footer = (
        <Footer
            title="Don't work?"
            action="Try another way"
            onPress={() => Linking.openURL("mailto:cuonggosu030297@gmail.com")} />
    );
    return (
        <Container {...{ footer }} >
            <Box padding="l" flex={1} justifyContent="center">
                <Text variant="title1" textAlign="center" marginBottom="l">
                    Forgot password
            </Text>
                <Text variant="body" textAlign="center" marginBottom="l">
                    Enter the email address associated with your account
            </Text>
                <Box>
                    <Box marginBottom="m">
                        <TextInput
                            icon="mail"
                            placeholder="Enter your email"
                            keyboardType="email-address"
                            autoCompleteType="email"
                            returnKeyType="go"
                            returnKeyLabel="go"
                            onChangeText={handleChange('email')}
                            onBlur={handleBlur('email')}
                            error={errors.email}
                            touched={touched.email}
                            onSubmitEditing={() => handleSubmit()}
                        />
                    </Box>

                    <Box alignItems="center">
                        <Button
                            variant="primary"
                            label="Reset password"
                            onPress={handleSubmit}
                        />
                    </Box>
                </Box>
            </Box>
        </Container>
    );
}

export default ForgotPassword;
