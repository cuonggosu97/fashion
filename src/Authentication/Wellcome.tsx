import React from "react";
import { StyleSheet, Image } from "react-native";
import { Box, Text } from "../components/Theme";
import { Button } from "../components";
import { Routes, StackNavigationProps } from "../components/Navigation";

const styles = StyleSheet.create({
    picture: {
        ...StyleSheet.absoluteFillObject,
        top: 20,
        width: undefined,
        height: undefined,
    },
})
const picture = {
    src: require('../assets/5.png'),
    width: 2000,
    height: 2000
}

export const assets = [picture.src]

const Wellcome = ({
    navigation
}: StackNavigationProps<Routes, "Wellcome">) => {
    return (
        <Box flex={1} backgroundColor="white">
            <Box
                flex={1} borderBottomRightRadius="xl"
                backgroundColor="grey"
                alignItems="center"
                justifyContent='flex-end'
            >
                <Image
                    source={picture.src}
                    style={styles.picture}
                    resizeMode='contain'
                />
            </Box>
            <Box flex={1} borderTopLeftRadius="xl" >
                <Box
                    backgroundColor="grey"
                    position="absolute"
                    top={0}
                    left={0}
                    right={0}
                    bottom={0}
                />
                <Box
                    backgroundColor="white"
                    borderTopLeftRadius="xl"
                    flex={1}
                    alignItems="center"
                    justifyContent="space-evenly"
                >
                    <Text variant="title2" >Let's get started</Text>
                    <Text variant="body" textAlign="center" >
                        Login to your account below or signup{"\n"}for an amazing experience
                    </Text>
                    <Button
                        variant="primary"
                        label="Have an account? Login"
                        onPress={() => navigation.navigate("Login")}
                    />
                    <Button
                        label="Join us, it's Free"
                        onPress={() => navigation.navigate("SignUp")}
                    />
                    <Button
                        variant="transparent"
                        label="Forgot password?"
                        onPress={() => navigation.navigate("ForgotPassword")}
                    />
                </Box>
            </Box>
        </Box >
    );
}

export default Wellcome;
